# == Schema Information
#
# Table name: tweets
#
#  id         :integer          not null, primary key
#  body       :text
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  user_id    :integer
#
# Foreign Keys
#
#  user_id  (user_id => users.id)
#
class Tweet < ApplicationRecord
  validates_length_of :body, minimum: 5, maximum: 180
  validate :is_unique
  belongs_to :user
  scope :by_not_affiliated, -> () { joins(user: :company).where(companies: {id: nil}) }

  def is_unique
    # revisar los tweets en las ultimas 24H
    # Tweets.where(body: self.body)
    tweets = Tweet.where("created_at > ?", Date.today - 1).where("user_id == ?", self.user_id).where("body == ?", self.body)
    self.errors.add(:base, "Tweet Duplicado") if tweets.exists?
  end
end
